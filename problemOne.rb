
require 'nokogiri'
require 'open-uri'

class Informations 


	PAGE = Nokogiri::HTML(open("https://xkcd.com/2137/"))   
	
	def printTitle
		'Title: "'+ PAGE.title+ '"'
	end

	def printImage
		'Image: "'+'http:'+ PAGE.css('#comic img').attr('src')+'"'
	end
	
end

informacao = Informations.new()
puts informacao.printTitle
puts informacao.printImage

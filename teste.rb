require_relative "problemOne"
require "test/unit"
 
class TestInformations< Test::Unit::TestCase
 
  def testTitleAndImage
   

    assert_equal('Title: "xkcd: Text Entry"',Informations.new().printTitle() )

    assert_equal('Image: "http://imgs.xkcd.com/comics/text_entry.png"',Informations.new().printImage() )
  

  end
 
end
